      SUBROUTINE farterj00(Z_R, Z_I, Ntheta, theta, 
     1                 J_R, J_I, rho_R, rho_I, NS, NN, coord, k, Iscplx)

      IMPLICIT NONE
      logical Iscplx

      INTEGER NN, Ntheta, NS
      REAL*8 Z_R(2,Ntheta), Z_I(2,Ntheta)
      REAL*8 theta(Ntheta)
      REAL*8 J_R(NS), J_I(NS), rho_R(Ns), rho_I(Ns)
      REAL*8 coord(2,NN)
      REAL*8 k

      CALL farterj00_F90(Z_R, Z_I, Ntheta, theta,  
     1              J_R, J_I, rho_R, rho_I, NS, NN, coord, k, Iscplx)

      RETURN
      END










