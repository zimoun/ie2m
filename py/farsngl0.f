      SUBROUTINE farsngl0(Z, Ntheta, theta, sol, 
     1                              NS, NN, coord, k, Iscplx)

      IMPLICIT NONE
      LOGICAL Iscplx

      INTEGER NN, Ntheta, NS
      REAL*8 Z_R(Ntheta), Z_I(Ntheta)
      REAL*8 theta(Ntheta)
      REAL*8 J_R(NN), J_I(NN)
      REAL*8 coord(2,NN)
      REAL*8 k

      COMPLEX*16 Z(Ntheta), sol(NN)
      INTEGER ii

Cf2py intent(in) theta
Cf2py intent(in) sol
Cf2py intent(in) NS
Cf2py intent(in) coord, k
Cf2py intent(out) Z
Cf2py integer intent(hide),depend(theta) :: Ntheta=shape(theta,1)
Cf2py integer intent(hide),depend(coord) :: NN=shape(coord,2)
Cf2py logical intent(hide) :: Iscplx

Cf2py external farsngl0_F90

      Iscplx = .false.

      do ii=1,NN
         J_R(ii) = real(sol(ii))
         J_I(ii) = imag(sol(ii))
      enddo

c$$$      CALL farsngl0_F90(Z_R, Z_I, Ntheta, theta, J_R, J_I, 
c$$$     1                              NS, NN, coord, k, Iscplx)             

      do ii=1,Ntheta
         Z(ii) = (1.0,0.0)*Z_R(ii) +  (0.0,1.0)*Z_I(ii)
      enddo

      RETURN
      END










