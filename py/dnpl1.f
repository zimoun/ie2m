      SUBROUTINE dnpl1(Z, theta, RNS, NN, coord, k)

      IMPLICIT NONE

      INTEGER NN, ii
      REAL*8 RNS
      REAL*8 Z_R(NN), Z_I(NN)
      COMPLEX*16 Z(NN)
      REAL*8 coord(2,NN)
      REAL*8 theta
      REAL*8 k

Cf2py intent(in) theta, RNS, k
Cf2py intent(in) coord
Cf2py intent(out) Z
Cf2py integer intent(hide),depend(coord) :: NN=shape(coord,1)

Cf2py external dnpl1_F90


      CALL dnpl1_F90(Z_R, Z_I, theta, RNS, NN, coord, k)

      do ii=1,NN
         Z(ii) =  (1.0,0.0)* Z_R(ii) + (0.0,1.0)* Z_I(ii)
      enddo


      RETURN
      END










