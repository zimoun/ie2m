function A = masscurve(a,Ns,coord,mid)
% call A = mass(a,coord) to form
% the mass matrix related to the following 
% bilinear form \int_{Ns,coord} a u v ds
% a can be ethier constant or values given at
% each node of at each mid-segment
% u v are constant over each segment
% {Ns, coord} is the mesh of the curve following
% the i2em toolbox specifications
% mid = 1 if a is given at the mid-segment
% mid = 0 otherwise

N = length(coord);

if length(a) == 1, a = a*ones(N,1); end


A = sparse(Ns,Ns);
for m = 1:Ns,
    ak = coord(:,m);
    ms = m + 1; if ms > N, ms = 1; end
    bk = coord(:,ms);
    lk = norm(bk-ak);
    if mid ~=1, % back to a value at each mid-segment
        am = (a(m)+a(ms))*0.5;
    else
        am = a(m);
    end
    A(m,m) = am*lk;
end
