% plane0 Assembles (opposite of) the contributions of a scalar
%        incident plane wave 
%  
%   Z = plane0(theta,NS,coord,k)
%   testing functions are constant on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INCIDENT PLANE WAVE PARAMETERS.
%      theta   -> angle between the x-axis and the (opposite) 
%                 of the propagation direction of the incident 
%                 wave
%      k       -> wavenumber of the incident wave
% 
%   INPUT PARAMETERS.
%      theta     - angle (in radians) 
%      NS, coord - input curve
%      k         - wavenumber (must be real)
%
%   OUTPUT PARAMETERS
%      Z - NS by 1 vector 

