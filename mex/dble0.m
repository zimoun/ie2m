% dble0 Assembles double-layer contributions
%  
%   Z = dble0(NSA,coordA,NSB,coordB,k)
%   trial functions on curve B are constant on each segment
%   testing functions on curve A are of the same type as on 
%   curve B
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INPUT PARAMETERS.
%      see sngl0
%
%   OUTPUT PARAMETERS
%      Z - NSA by NSB complex matrix giving the double-layer
%          interactions   
