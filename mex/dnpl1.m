% dnpl1 Assembles (opposite of) the contributions of the normal
%       derivative of a scalar incident plane wave 
%  
%   Z = dnpl1(theta,NS,coord,k)
%   testing functions are continuous on the input curve
%   and linear-affine on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INCIDENT PLANE WAVE PARAMETERS.
%      see plane0
% 
%   INPUT PARAMETERS.
%      see plane0
%
%   OUTPUT PARAMETERS
%      Z - NN by 1 vector
%
%   IMPORTANT CAUTION.
%      see sngl1 

