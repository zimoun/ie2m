% dnpl0 Assembles (opposite of) the contributions of a the
%       normal derivative of a scalar incident plane wave 
%  
%   Z = dnpl0(theta,NS,coord,k)
%   testing functions are constant on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INCIDENT PLANE WAVE PARAMETERS.
%      see plane0
% 
%   INPUT PARAMETERS.
%      see plane0
%
%   OUTPUT PARAMETERS
%      Z - NS by 1 vector 

