% mass0 Assembles mass contributions 
%  
%   Z = mass0(NS,coord)
%   trial functions are constant on each segment
%   same form for testing functions 
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INPUT PARAMETERS.
%      see mass1
%
%   OUTPUT PARAMETERS
%      Z - NS by NS matrix 
%          

